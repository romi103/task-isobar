const fetch = require("node-fetch");

const firstEndPoint = "https://baconipsum.com/api/?type=all-meat&paras=3&start-with-lorem=1&format=json";
const secondEndPoint = "https://baconipsum.com/api/?type=meat-and-filler&paras=5&format=json";


function fetchData(path1, pat2) {
    const first = fetch(path1).then(res => res.json());
    const second = fetch(pat2).then(res => res.json());
    let joinedString = "";

    Promise.all([first, second]).then((res) => {
        joinedString = res.join();
        console.log(joinedString)
    }).catch(err => console.error("An error occured!"))
}



fetchData(firstEndPoint, secondEndPoint);